import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          title: Center(
            child: Text('my first app'),
          ),
          backgroundColor: Color.fromRGBO(63, 44, 180, 1),
        ),
        body: Center(
          child: Text('my new app'),
        ),
      ),
    );
  }
}
